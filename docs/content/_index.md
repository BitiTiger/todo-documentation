---
title: "Home"
date: 2020-10-22T18:36:58-04:00
draft: false
weight: 1
hide:
- footer
- nextpage
---

## What is To-Do List?

To-Do List tracks your tasks and deadlines. Unlike alternatives, To-Do List is designed for use in a terminal. It is lightweight and offers great flexibility. All tasks are kept in a single database file for easy synchronization with a third party tool like Nextcloud or Dropbox.

## Website Outline

- [Homepage](/)
  - [Description](/#what-is-to-do-list)
  - [Outline](/#outline)
- [Getting Started](/getting-started/)
  - [How to install](/getting-started/#how-to-install)
  - [How to use the program](/getting-started/#how-to-use-the-program)
  - [How to troubleshoot](/getting-started/#how-to-troubleshoot)
  - [How to contribute](/getting-started/#how-to-contribute)
- [Configuration](/configuration)
  - [Dependencies](/configuration/#dependencies)
  - [Installing](/configuration/#installing)
  - [Upgrading](/configuration/#upgrading)
  - [Removing](/configuration/#removing)
- [Usage](/usage)
  - [Launching](/usage/#launching)
  - [Exiting](/usage/#exiting)
  - [Keybindings](/usage/#keybindings)
  - [Add a task](/usage/#add-a-task)
  - [Complete a task](/usage/#complete-a-task)
  - [Remove a task](/#remove-a-task)
  - [Edit a task](/usage/#edit-a-task)
    - [Change the name](/usage/#change-the-name)
    - [Change the location](/usage/#change-the-location)
    - [Change the date](/usage/#change-the-date)
    - [Delete the task](/usage/#delete-the-task)
    - [Cancel edits](/usage/#cancel-edits)
- [FAQ](/faq)
- [Contributing](/contributing)
  - [Writing code](/contributing/#writing-code)
  - [Writing documentation](/contributing/#writing-documentation)
  - [Sharing with others](/contributing/#sharing-with-others)
- [Licenses](/license)
  - [To-Do List License](/license/#to-do-list-license)
  - [Hugo License](/license/#hugo-license)
  - [DocPort License](/license/#docport-license)
