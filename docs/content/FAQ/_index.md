---
title: "FAQ"
date: 2020-10-23T14:16:46-04:00
draft: false
weight: 5
hide: footer
---

## Can I use this program on Windows?

Yes. You need to install [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10), and then follow the installation instructions inside a Linux environment.

## Can I use this program on MacOS?

To-Do List does not officially support macOS, but it should be possible. Follow the install instructions inside the Terminal app.

## Can I use this program on [random operating system] instead of Linux?

To-Do List only supports Linux, but it might be possible. You need to have BASH, a Python environment, a UNIX-like file structure, and UNIX-like commands such as `chmod`, `ln` and `rm`.

## Can I embed this program inside my project?

Yes, but you must be in compliance with the [software license](/license).

## Is there a graphical version of this project?

Not right now. A graphical version is on my list of things to make. Do not expect this to be completed soon as I have many other things being worked on.

## How do I change the language?

Right now, the program only supports English. There are currently no plans to add foreign language support since this project has very few users. If you wish to add support for foreign languages, see the [section about contributing](/contributing/).
