---
title: "Contributing"
date: 2020-10-23T14:36:19-04:00
draft: false
weight: 6
hide: footer
---

First of all, thank you for your interest in this project. You can contribute to this project in several ways:

- Help with bug fixes and feature implementation
- Edit the documentation
- Share this program with others

## Writing code

This project employs KDE's ["scratch your own itch"](https://community.kde.org/Get_Involved/development#Choose_what_to_do) policy. Try fixing something that bothers you. If you do not have an itch, try [fixing an issue](https://gitlab.com/caton101/todo-list/-/issues?label_name%5B%5D=Good+First+Issue) tagged with `Good First Issue`.

Please follow these steps to contribute:

- [Create an issue](https://gitlab.com/caton101/todo-list/-/issues)
- [Fork the repository](https://gitlab.com/caton101/todo-list)
- Edit the source code
- Perform testing
- [Submit a merge request](https://gitlab.com/caton101/todo-list/-/merge_requests)

**Note:**
Please ensure that your code works and does not cause random crashes. Merge requests with faulty code will not be accepted.

## Writing documentation

This project is aimed at users who want a To-Do List application that is simple to learn and use. All documentation assumes the audience is an end user. As such, all pages should contain a clear layout and take minimal effort to navigate. To contribute, please follow these steps:

- [Create an issue](https://gitlab.com/caton101/todo-documentation/-/issues)
- [Fork the repository](https://gitlab.com/caton101/todo-documentation)
- Edit the page you wish to modify
- [Submit a merge request](https://gitlab.com/caton101/todo-documentation/-/merge_requests)

**Note:**
It is not necessary to build the site using Hugo, but the extra testing is appreciated.

## Sharing with others

Sharing is how this project gains both end users and contributors. You do not have to be skilled in programming or writing documentation to spread the word. Just make a blog post or share this program on social media.
