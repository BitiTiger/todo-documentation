---
title: "Configuration"
date: 2020-10-23T13:35:20-04:00
draft: false
weight: 3
hide: footer
---

## Dependencies

- [A Linux distribution](https://en.wikipedia.org/wiki/List_of_Linux_distributions)
- [A terminal emulator](https://en.wikipedia.org/wiki/List_of_terminal_emulators) (xterm, Gnome Terminal, Konsole, etc.)
- [Python 3 or higher](https://www.python.org/downloads/)
- [The blessed Python Package](https://pypi.org/project/blessed/)

## Installing

1. Clone the repository:
`git clone https://gitlab.com/caton101/todo-list`

2. Enter the downloaded directory:
`cd todo-list`

3. Mark the install script as executable:
`chmod +x install`

4. Run the installer:
`./install`

{{< asciinema videoID="asciicast-367790" videoSRC="https://asciinema.org/a/367790.js" >}}

## Upgrading

1. Clone the repository:
`git clone https://gitlab.com/caton101/todo-list`

2. Enter the downloaded directory:
`cd todo-list`

3. Mark the install script as executable:
`chmod +x install`

4. Mark the uninstall script as executable:
`chmod +x uninstall`

5. Run the uninstaller:
`./uninstall`

6. Run the installer:
`./install`

{{< asciinema videoID="asciicast-367792" videoSRC="https://asciinema.org/a/367792.js" >}}

## Removing

1. Clone the repository:
`git clone https://gitlab.com/caton101/todo-list`

2. Enter the downloaded directory:
`cd todo-list`

3. Mark the uninstall script as executable:
`chmod +x uninstall`

4. Run the uninstaller:
`./uninstall`

{{< asciinema videoID="asciicast-367791" videoSRC="https://asciinema.org/a/367791.js" >}}
