---
title: "Licenses"
date: 2020-10-23T14:32:52-04:00
draft: false
weight: 7
hide: footer
---

## To-Do List License

This project is licensed under the [BSD 2 Clause License](https://opensource.org/licenses/BSD-2-Clause).

**Note:**
To-Do List's license is subject to change. Please check the [official repository](https://gitlab.com/caton101/todo-list/-/blob/master/LICENSE) for current licensing details.

## Hugo License

This project uses Hugo to convert the documentation into a static website.

- Hugo versions 0.15 and higher are licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0).

- Older versions of Hugo are licensed under the [Simple Public License](https://opensource.org/licenses/Simple-2.0).

**Note:**
Hugo's license is subject to change. Please check the [official website](https://gohugo.io/about/license/) for current licensing details.

## DocPort License

This website was generated using the [DocPort](https://docport.netlify.app/docport-theme/) theme for Hugo.

DocPort is licensed under the [MIT License](https://mit-license.org/).

**Note:**
DocPort's license is subject to change. Please check the [official repository](https://github.com/vjeantet/hugo-theme-docport/blob/master/LICENSE) for current licensing details.
